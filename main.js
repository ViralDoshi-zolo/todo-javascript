const input=document.querySelector("textarea")
const btn=document.querySelector(".save")

btn.addEventListener('click',addInList);
input.addEventListener('keyup', (e)=>{
    (e.key === 13 ? addInList(e) : null);
})

function addInList(e){
    const completed=document.querySelector(".done");
    const notCompleted=document.querySelector(".pending");
    const list=document.createElement('li');
    const doneBtn=document.createElement('button');
    const deleteBtn=document.createElement('button');

    
    deleteBtn.innerHTML='<i class="fa fa-times" aria-hidden="true"></i>';
    doneBtn.innerHTML='<button class="mark">Mark as Done</button>';

    if(input.value!==''){
        list.textContent=input.value;
        input.value='';
        notCompleted.appendChild(list);
      
        list.appendChild(deleteBtn);
        list.appendChild(doneBtn);
    }

    doneBtn.addEventListener('click', function(){
        const parent=this.parentNode;
        parent.remove();
        completed.appendChild(parent);
        doneBtn.style.display='none';
    });

    deleteBtn.addEventListener('click',function(){
        const parent=this.parentNode;
        parent.remove();
    });

}
